# -*- coding:utf-8 -*-
from __future__ import absolute_import, division, print_function, unicode_literals

import torch
from torch import nn
from torch.nn.utils.rnn import pack_padded_sequence, pad_packed_sequence

from pytorch_pretrained_bert import BertModel, BertConfig

from torchcrf import CRF
import gluonnlp as nlp

class Kobert_CRF(nn.Module):
    """
    KOBERT with CRF
    """
    
    def __init__(self, num_classes, model_file, vocab_file, bert_config) -> None:
        """
        @params:
            model_file: BERT Pretrained Model weights. eg. pytorch_kobert_2439f391a6.params
            vocab_file: BERT vocab file. eg.  kobertvocab_f38b8a4d6d.json
        """
        super(Kobert_CRF, self).__init__()
        self.model_file = model_file
        self.vocab_file = vocab_file
        self.bert_config = bert_config
        self.num_classes = num_classes
        self.bert, self.vocab = self.get_kobert_model(self.model_file, self.vocab_file, self.bert_config)
        print('model loaded..')
        self.dropout = nn.Dropout(bert_config['hidden_dropout_prob'])
        self.position_wise_ff = nn.Linear(bert_config['hidden_size'], self.num_classes)

        self.crf = CRF(num_tags = self.num_classes, batch_first = True)
        
    def get_kobert_model(self, model_file, vocab_file,bert_config, ctx="cpu"):
        bertmodel = BertModel(config=BertConfig.from_dict(bert_config))
        bertmodel.load_state_dict(torch.load(model_file))
        device = torch.device(ctx)
        bertmodel.to(device)
        bertmodel.eval()
        vocab_b_obj = nlp.vocab.BERTVocab.from_json(
            open(vocab_file, 'rt').read())
        return bertmodel, vocab_b_obj
    
    def forward(self, input_ids, token_type_ids = None, tags = None):
        # token_type_ids: Segment token indices to indicate first and second portions of the inputs. 
        #Indices are selected in [0, 1]: 0 corresponds to a sentence A token, 1 corresponds to a sentence B token 
        #attention_mask Mask to avoid performing attention on padding token indices. Mask values selected in [0, 1]
        attention_mask = input_ids.ne(self.vocab.token_to_idx[self.vocab.padding_token]).float()
        all_encoder_layers, pooled_output = self.bert(input_ids = input_ids, token_type_ids = token_type_ids, attention_mask = attention_mask)
        last_encoder_layer = all_encoder_layers[-1]
        last_encoder_layer = self.dropout(last_encoder_layer)
        emissions = self.position_wise_ff(last_encoder_layer)
        if tags is not None:
            log_likelihood  = self.crf(emissions, tags)
            sequence_of_tags = self.crf.decode(emissions)
            return log_likelihood, sequence_of_tags
        else:
            sequence_of_tags = self.crf.decode(emissions)
            return sequence_of_tags