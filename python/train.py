# -*- coding:utf-8 -*-

import torch
import torch.optim as optim
from pytorch_transformers import AdamW, WarmupLinearSchedule
from torch.utils.tensorboard import SummaryWriter
from utils import CheckpointManager, SummaryManager
from utils import set_seed
import os
import sys
from utils import load_config, f1_score, save_cr_and_cm
from data_manager import DataManager
from torch.utils.data import DataLoader
from kobert_crf_model import Kobert_CRF
import logging
import json
from tqdm import tqdm, trange
from pathlib import Path
import numpy as np
import argparse

logger = logging.getLogger()
stderr_log_handler = logging.StreamHandler()
logger.addHandler(stderr_log_handler)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
stderr_log_handler.setFormatter(formatter)



def save_mechanism(log_dir, checkpoint_dir):
    log_dir = Path(log_dir)
    checkpoint_dir = Path(checkpoint_dir)
    _tb_writer = SummaryWriter(log_dir/ 'runs')
    _checkpoint_manager = CheckpointManager(checkpoint_dir)
    _summary_manager = SummaryManager(log_dir)
    return _tb_writer, _checkpoint_manager, _summary_manager


def define_optimizer(model, gradient_accumulation_steps, learning_rate, adam_eps, warmup_steps, epochs):
    """
    decide parameters to learn
    define scheduler if want
    Use 1 gpu for default
    """
    param_optimizer = list(model.named_parameters())
    print("Number of parameter matrix = ", len(param_optimizer))
    
    no_decay = ['bias', 'LayerNorm.bias', 'LayerNorm.weight']
    optimizer_grouped_parameters = [
        {'params': [p for n, p in param_optimizer if not any(nd in n for nd in no_decay)], 'weight_decay': .01},
        {'params': [p for n, p in param_optimizer if any(nd in n for nd in no_decay)], 'weight_decay': .00},
    ]

    t_total = len(tr_dl) // gradient_accumulation_steps * epochs
    
    # Optimizer
    _opt = AdamW(optimizer_grouped_parameters, lr = learning_rate, eps = adam_eps)
    # Scheduler
    _sche = WarmupLinearSchedule(_opt, warmup_steps=warmup_steps, t_total=t_total)
        # WarmupLinearSchedule: Linearly increases learning rate from 0 to 1 over warmup fraction of training steps. Linearly decreases learning rate from 1. to 0. over remaining 1
    return _opt, _sche
    
    
def set_dataLoaders(train_data_file, tags, tag_map, tokenizer_model, max_length, batch_size):
    """
    Data는 train, valid, category라는 3개의 key를 가짐
    """
    
    _t_dm = DataManager(train_data_file = train_data_file,
                        max_length = max_length,
                        tags = tags, 
                        tag_map = tag_map,
                        tokenizer_model = tokenizer_model, 
                        data_type='train')
    
    _v_dm = DataManager(train_data_file = train_data_file,
                        max_length = max_length,
                        tags = tags, 
                        tag_map = tag_map,
                        tokenizer_model = tokenizer_model,
                        data_type = 'valid')
    
    _t_dl = DataLoader(_t_dm, batch_size=batch_size, shuffle=True, num_workers=4, drop_last=False)
   
    _v_dl = DataLoader(_v_dm, batch_size=batch_size, shuffle=False, num_workers=4, drop_last=False)
    return _t_dm, _t_dl, _v_dm, _v_dl
    


def evaluate(model, val_dl, device, eval_type = 'saving', prefix="NER", n_sample = 2000):
    """ evaluate accuracy and return result """
    results = {}
    #device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')

    # Eval!
    logger.info("***** Running evaluation {} *****".format(prefix))
    eval_loss = 0.0
    nb_eval_steps = 0

    list_of_y_real = []
    list_of_pred_tags = []
    y_real_list = []
    y_pred_list = []
    input_list = []
    length_list = []
    count_correct = 0
    total_count = 0
    total_sentence=  0
    for batch in tqdm(val_dl, desc="Evaluating for {}".format(eval_type) ):
        model.eval()
        x_input, token_type_ids, y_real, length = map(lambda elm: elm.to(device),  batch[:4])
        with torch.no_grad():
            inputs = {'input_ids': x_input,
                      'token_type_ids': token_type_ids,
                      'tags': y_real}
            log_likelihood, sequence_of_tags = model(**inputs)

            eval_loss += -1 * log_likelihood.float().item()
        nb_eval_steps += 1
        
        y_real_list.extend(y_real)
        y_pred_list.extend(sequence_of_tags)
        length_list.extend(length)
        input_list.extend(x_input)
        total_sentence += len(y_real)

        y_real = y_real.to(device)
        sequence_of_tags = torch.tensor(sequence_of_tags).to(device)
        count_correct += (sequence_of_tags == y_real).float()[y_real != 2].sum()  # 0,1,2,3 -> [CLS], [SEP], [PAD], [MASK] index
        total_count += len(y_real[y_real != 2])

        for seq_elm in y_real.tolist():
            list_of_y_real += seq_elm

        for seq_elm in sequence_of_tags.tolist():
            list_of_pred_tags += seq_elm

    eval_loss = eval_loss / nb_eval_steps
    acc = (count_correct / total_count).item()  # tensor -> float
    result = {"eval_acc": acc, "eval_loss": eval_loss}
    results.update(result)
    return results, list_of_y_real, list_of_pred_tags, y_real_list[:2000], y_pred_list[:2000], input_list[:2000], length_list[:2000]


def train(tr_dl, val_dl, train_config, device):
    
    global_step = 0
    tr_loss, logging_loss = 0.0, 0.0
    best_dev_acc, best_dev_loss = 0.0, 99999999999.0
    best_steps = 0
    model.zero_grad()
    
    checkpoint_dir = Path(train_config['checkpoint_dir'])
    model_summary_dir = Path(train_config['model_summary_dir'])
    
    tag_id_map = {v:k for k, v in tr_dm.tag_map.items()}
    
    train_iterator = trange(int(train_config['epochs']), desc="Epoch")
    for _epoch, _ in enumerate(train_iterator):
        epoch_iterator = tqdm(tr_dl, desc="Iteration")
        epoch = _epoch
        for step, batch in enumerate(epoch_iterator):
            model.train()
            x_input, token_type_ids, y_real, length = map(lambda elm: elm.to(device), batch[:4])
            log_likelihood, sequence_of_tags = model(x_input, token_type_ids, y_real)
            # loss: negative log-likelihood
            loss = -1 * log_likelihood
            print('loss = ', loss)
            if n_gpu > 1:
                loss = loss.mean()  # mean() to average on multi-gpu parallel training
            if train_config['gradient_accumulation_steps'] > 1:
                loss = loss / train_config['gradient_accumulation_steps']
            loss.backward()
            torch.nn.utils.clip_grad_norm_(model.parameters(), train_config['max_grad_norm'])
            tr_loss += loss.item()
            if (step + 1) % train_config['gradient_accumulation_steps'] == 0:
                optimizer.step()
                scheduler.step()  # Update learning rate schedule
                model.zero_grad()
                global_step += 1
                with torch.no_grad():
                    sequence_of_tags = torch.tensor(sequence_of_tags).to(device)
                    mb_acc = (sequence_of_tags == y_real).float()[y_real != tr_dm.tag_map['[PAD]']].mean()
                tr_acc = mb_acc.item()
                tr_loss_avg = tr_loss / global_step
                tr_summary = {'loss': tr_loss_avg, 'acc': tr_acc}
                # if step % 50 == 0:
                print('epoch : {}, global_step : {}, tr_loss: {:.3f}, tr_acc: {:.2%}'.format(epoch + 1, global_step, tr_summary['loss'], tr_summary['acc']))
                # training & evaluation log
                if train_config['logging_steps'] > 0 and global_step % train_config['logging_steps'] == 0:
                    if train_config['evaluate_during_training']:  # Only evaluate when single GPU otherwise metrics may not average well
                        
                        eval_summary, list_of_y_real, list_of_pred_tags, y_real_list, y_pred_list, input_list, list_of_length = evaluate(model, val_dl, device, 'logging')
                        for i in range(100):
                            id = tr_dm.tokenizer.DecodePieces([tr_dm.tokenizer.IdToPiece(x.item()) for x in input_list[i].cpu().data.numpy()]) + '>>>' + 'token: ' + ' '.join([tr_dm.tokenizer.IdToPiece(x.item()) for x in input_list[i].cpu().data.numpy()])
                            try:
                                tb_writer.add_text(id, 'pred: ' + '  '.join([tag_id_map[x]for x in y_pred_list[i]]), global_step)
                            except:
                                tb_writer.add_text(id, 'pred: Error occurred ')
                            tb_writer.add_text(id, 'label: ' + '  '.join([tag_id_map[x] for x in y_real_list[i].cpu().data.numpy()]), global_step)
                            
                            
                        for tag in va_dm.tags:
                            rec, prec, f1, support = f1_score(y_real_list[:1000], y_pred_list[:1000], list_of_length[:1000], tag, va_dm.tag_map)
                            tb_writer.add_scalars(tag, {'recall': rec, 'precision': prec, 'f1 score': f1}, global_step)
                        
                        tb_writer.add_scalar('lr', scheduler.get_lr()[0], global_step)
                        tb_writer.add_scalars('loss', {'train': (tr_loss - logging_loss) / train_config['logging_steps'], 'val': eval_summary["eval_loss"]}, global_step)
                        tb_writer.add_scalars('acc', {'train': tr_acc, 'val': eval_summary["eval_acc"]}, global_step)
                        print("eval acc: {}, loss: {}, global steps: {}".format(eval_summary['eval_acc'], eval_summary['eval_loss'], global_step))
                    print("Average loss: {} at global step: {}".format((tr_loss - logging_loss) / train_config['logging_steps'], global_step))
                    logging_loss = tr_loss
                # save model
                if train_config['save_steps'] > 0 and global_step % train_config['save_steps'] == 0:
                    eval_summary, list_of_y_real, list_of_pred_tags, y_real_list, y_pred_list, _, list_of_length = evaluate(model, val_dl, device, 'saving')
                    # Save model checkpoint
                    checkpt_dir = checkpoint_dir / 'epoch-{}'.format(epoch + 1)
                    if not os.path.exists(checkpoint_dir):
                        os.makedirs(checkpoint_dir)
                    print("Saving model checkpoint to %s", checkpt_dir)
                    state = {'global_step': global_step + 1,
                             'model_state_dict': model.state_dict(),
                             'opt_state_dict': optimizer.state_dict()}
                    summary = {'train': tr_summary, 'eval': eval_summary}
                    summary_manager.update(summary)
                    print("summary: ", summary)
                    summary_manager.save('summary.json')
                    # Save
                    is_best = eval_summary["eval_acc"] >= best_dev_acc  # acc 기준 (원래는 train_acc가 아니라 val_acc로 해야)
                    if is_best:
                        best_dev_acc = eval_summary["eval_acc"]
                        best_dev_loss = eval_summary["eval_loss"]
                        best_steps = global_step
                        # if args.do_test:
                        # results_test = evaluate(model, test_dl, test=True)
                        # for key, value in results_test.items():
                        #     tb_writer.add_scalar('test_{}'.format(key), value, global_step)
                        # logger.info("test acc: %s, loss: %s, global steps: %s", str(eval_summary['eval_acc']), str(eval_summary['eval_loss']), str(global_step))
                        checkpoint_manager.save_checkpoint(state, 'best-epoch-{}-step-{}-acc-{:.3f}.bin'.format(epoch + 1, global_step, best_dev_acc))
                        print("Saving model checkpoint as best-epoch-{}-step-{}-acc-{:.3f}.bin".format(epoch + 1, global_step, best_dev_acc))
                        # print classification report and save confusion matrix
                        cr_save_path = model_summary_dir / 'best-epoch-{}-step-{}-acc-{:.3f}-cr.csv'.format(epoch + 1, global_step, best_dev_acc)
                        cm_save_path = model_summary_dir / 'best-epoch-{}-step-{}-acc-{:.3f}-cm.png'.format(epoch + 1, global_step, best_dev_acc)
                        save_cr_and_cm(va_dm, list_of_y_real, list_of_pred_tags, y_real_list, y_pred_list, list_of_length, cr_save_path=cr_save_path, cm_save_path=cm_save_path)
                    else:
                        torch.save(state, checkpoint_dir / 'model-epoch-{}-step-{}-acc-{:.3f}.bin'.format(epoch + 1, global_step, eval_summary["eval_acc"]))
                        print("Saving model checkpoint as model-epoch-{}-step-{}-acc-{:.3f}.bin".format(epoch + 1, global_step, eval_summary["eval_acc"]))
    tb_writer.close()
    print("global_step = {}, average loss = {}".format(global_step, tr_loss / global_step))

    
if __name__ == '__main__':
    
    # Load Config
    bert_config, data_config, train_config = load_config()
    
    # Save mechanism: tensorboard, save
    tb_writer, checkpoint_manager, summary_manager = save_mechanism(train_config['log_dir'], train_config['checkpoint_dir'])

    
    # Set DataLoader
    tr_dm, tr_dl, va_dm, va_dl = set_dataLoaders(data_config['train_data_file'], 
                                                 data_config['tags'], 
                                                 data_config['tag_map'],
                                                 data_config['tokenizer_model'],
                                                 train_config['max_length'],
                                                 train_config['batch_size'])    

    # Define Model => affects trainer
    model = Kobert_CRF(data_config['num_classes'], 
                       data_config['bert_pretrained_model_file'], 
                       data_config['vocab_file'], 
                       bert_config)
    
    
    # Define Optimizer
    optimizer, scheduler = define_optimizer(model, 
                                            train_config['gradient_accumulation_steps'],
                                            train_config['learning_rate'],
                                            train_config['adam_epsilon'],
                                            train_config['warmup_steps'],
                                            train_config['epochs'])

    # Move model to GPU
    os.environ['CUDA_DEVICE_ORDER'] = 'PCI_BUS_ID'
    if isinstance(train_config['gpu_id'], int):
        device = torch.device('cuda:{}'.format(train_config['gpu_id']))
        model.to(device)
    elif isinstance(train_config['gpu_id'], list):
        torch.cuda.set_device(3)
        n_gpu = len(train_config['gpu_id']) # Multi GPU가 잘 안됨
        os.environ['CUDA_VISIBLE_DEVICES'] = ','.join(str(x) for x in train_config['gpu_id'])
        device = torch.device('cuda')
        model = torch.nn.DataParallel(model, device_ids = train_config['gpu_id']).cuda() # Data Parallel을 하기 위해
    #n_gpu = torch.cuda.device_count() # Multi GPU가 잘 안됨
    n_gpu = 2
    #set_seed(n_gpu)  # Added here for reproductibility (even between python 2 and 3)
    
    
    
    # Train
    train(tr_dl, va_dl, train_config, device)